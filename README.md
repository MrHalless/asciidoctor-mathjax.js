# Server side MathJax rendering for Asciidoctor.js/Antora
:version: 0.0.8

@djencks/asciidoctor-mathjax provides an asciidoctor.js extension to replace the built-in html asciidoctor stem processing wth server-side rendering of asciimath and latexmath expressions using the mathjax 3 javascript library.
Output is inline svg included in the final html document.
The intent is that configuration is identical to built-in stem processing.

NOTE: for more complete, better formatted README, see https://gitlab.com/djencks/asciidoctor-mathjax.js/-/blob/master/README.adoc.

## Installation

Available through npm as @djencks/asciidoctor-mathjax.

The project git repository is https://gitlab.com/djencks/asciidoctor-mathjax.js

## Usage in asciidoctor.js

see https://gitlab.com/djencks/asciidoctor-mathjax.js/-/blob/master/README.adoc

## Usage in Antora

I've mostly used this with Antora upgraded to use asciidoctor 2, but it seems to work fine using released versions of Antora.

* Install this extension so it is visible to Antora.
* List the extension in the antora playbook.

see https://gitlab.com/djencks/asciidoctor-mathjax.js/-/blob/master/README.adoc

## Antora Example project

A fairly complete example project showing all the possible stem locations and configurations is under extensions/mathjax in `https://gitlab.com/djencks/simple-examples`.
