/* eslint-env mocha */

const mathJaxProcessor = require('../lib/asciidoctor-mathjax')
const asciidoctor = require('@asciidoctor/core')()

const chai = require('chai')
const { expect } = require('chai')
const { initSnapshotManager } = require('mocha-chai-jest-snapshot')
chai.use(initSnapshotManager)

const PRINT = false

describe('stem mathjax tests', () => {
  it('asciidoc default stem block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`

A matrix can be written as 

[stem]
++++
[[a,b],[c,d]]((n),(k))
sqrt(4) = 2
sqrt(9) = 3
++++
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciidoc default prose block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:

A matrix can be written as stem:[[[a,b\\],[c,d\\]\\]((n),(k))].
A square root looks like stem:[sqrt(4) = 2].
Another square root looks like stem:[sqrt(9) = 3].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciidoc default prose block with attribute substitution test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:
:a: a
:b: b
:c: c
:d: d
:n: n
:k: k
:four: 4
:two: 2
:squareroot: sqrt
:expr: sqrt(9)=3

A matrix can be written as stem:a[[[{a},{b}\\],[{c},{d}\\]\\](({n}),({k}))].
A square root looks like stem:a[{squareroot}({four}) = {two}].
Another square root looks like stem:a[{expr}].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciidoc default prose list test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:

* A matrix can be written as stem:[[[a,b\\],[c,d\\]\\]((n),(k))].
A square root looks like stem:[sqrt(4) = 2]
* Another square root looks like stem:[sqrt(9) = 3]
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciidoc default dlist test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:

no stem::
no stem description

no stem::
some stem stem:[sqrt(1)]

no stem::
some stem stem:[sqrt(1)]::
stem:[sqrt(4) = 2]

example stem:[[[a,b\\],[c,d\\]\\]((n),(k))]::
no stem description

example stem:[[[a,b\\],[c,d\\]\\]((n),(k))]::
A fraction looks like stem:[sqrt(4) = 2].
+
[stem]
++++
sqrt(9) = 3
++++
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciidoc default table test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:

.Math Table
[cols="3*"]
|===

|A matrix can be written as stem:[[[a,b\\],[c,d\\]\\]((n),(k))].
|A square root looks like stem:[sqrt(4) = 2]
|Another square root looks like stem:[sqrt(9) = 3]
|===
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciidoc default table with head, foot test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:

.Math Table
[cols="3*",options="header,footer"]
|===
|Header matrix: stem:[[[a,b\\],[c,d\\]\\]((n),(k))]
|Header square root: stem:[sqrt(4) = 2]
|Header square root: stem:[sqrt(9) = 3]

|A matrix can be written as stem:[[[a,b\\],[c,d\\]\\]((n),(k))].
|A square root looks like stem:[sqrt(4) = 2]
|Another square root looks like stem:[sqrt(9) = 3]

|Footer matrix: stem:[[[a,b\\],[c,d\\]\\]((n),(k))]
|Footer square root: stem:[sqrt(4) = 2]
|Footer square root: stem:[sqrt(9) = 3]

|===
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciidoc default table test with asciidoc cells', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:

.Math Table
[cols="3*a"]
|===

|A matrix can be written as stem:[[[a,b\\],[c,d\\]\\]((n),(k))].
a|A square root looks like stem:[sqrt(4) = 2]
|Another square root looks like stem:[sqrt(9) = 3]
|===
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciidoc default table with head, foot test with asciidoc cells', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:

.Math Table
[cols="3*a",options="header,footer"]
|===
|Header matrix: stem:[[[a,b\\],[c,d\\]\\]((n),(k))]
|Header square root: stem:[sqrt(4) = 2]
|Header square root: stem:[sqrt(9) = 3]

|A matrix can be written as stem:[[[a,b\\],[c,d\\]\\]((n),(k))].
|A square root looks like stem:[sqrt(4) = 2]
|Another square root looks like stem:[sqrt(9) = 3]

|Footer matrix: stem:[[[a,b\\],[c,d\\]\\]((n),(k))]
|Footer square root: stem:[sqrt(4) = 2]
|Footer square root: stem:[sqrt(9) = 3]

|===
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciidoc default section test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
= The Title
:stem:

== A matrix can be written as stem:[[[a,b\\],[c,d\\]\\]((n),(k))].

== A square root looks like stem:[sqrt(4) = 2].

=== Another square root looks like stem:[sqrt(9) = 3].

`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('latexmath stem block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath

Some vector components are

[stem]
++++
u=\\frac{-y}{x^2+y^2}\\,,\\quad
v=\\frac{x}{x^2+y^2}\\,,\\quad\\text{and}\\quad
w=0\\,.
++++
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('latexmath prose block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath

An integral can be written as stem:[\\iint xy^2\\,dx\\,dy ].
A fraction looks like stem:[u=\\frac{-y}{x^2+y^2}].
Another fraction looks like stem:[v=\\frac{x}{x^2+y^2}].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('latexmath prose block with attribute substitution test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath
:x: x
:y: y
:iint: \\iint
:expr: v=\\frac{{x}}{x^2+y^2}

An integral can be written as stem:a[{iint} {x}{y}^2\\,dx\\,dy ].
A fraction looks like stem:a[u=\\frac{-{y}}{{x}^2+{y}^2}].
Another fraction looks like stem:a[{expr}].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('latexmath prose list test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath

* An integral can be written as stem:[\\iint xy^2\\,dx\\,dy].
A fraction looks like stem:[u=\\frac{-y}{x^2+y^2}].
* Another fraction looks like stem:[v=\\frac{x}{x^2+y^2}].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('latexmath dlist test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath

no stem::
no stem description

no stem::
some stem stem:[\\frac{x}{x^2+y^2}]

no stem::
some stem stem:[xy^2]::
stem:[\\iint xy^2\\,dx\\,dy]

example stem:[u=\\frac{-y}{x^2+y^2}]::
no stem description

example stem:[u=\\frac{-y}{x^2+y^2}]::
A fraction looks like stem:[u=\\frac{-y}{x^2+y^2}].
+
[stem]
++++
v=\\frac{x}{x^2+y^2}
++++
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('latexmath table test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath

.Math Table
[cols="3*"]
|===

|An integral can be written as stem:[\\iint xy^2\\,dx\\,dy ].
|A fraction looks like stem:[u=\\frac{-y}{x^2+y^2}].
|Another fraction looks like stem:[v=\\frac{x}{x^2+y^2}].
|===
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('latexmath table with head, foot test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath

.Math Table
[cols="3*",options="header,footer"]
|===
|Header integral can be written as stem:[\\iint xy^2\\,dx\\,dy ].
|Header fraction looks like stem:[u=\\frac{-y}{x^2+y^2}].
|Header another fraction looks like stem:[v=\\frac{x}{x^2+y^2}].

|An integral can be written as stem:[\\iint xy^2\\,dx\\,dy ].
|A fraction looks like stem:[u=\\frac{-y}{x^2+y^2}].
|Another fraction looks like stem:[v=\\frac{x}{x^2+y^2}].

|Footer integral can be written as stem:[\\iint xy^2\\,dx\\,dy ].
|Footer fraction looks like stem:[u=\\frac{-y}{x^2+y^2}].
|Footer another fraction looks like stem:[v=\\frac{x}{x^2+y^2}].

|===
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('latexmath section test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
= The Title
:stem: latexmath

== An integral can be written as stem:[\\iint xy^2 dx dy ].

== A fraction looks like stem:[u=\\frac{-y}{x^2+y^2}]

=== Another fraction looks like stem:[v=\\frac{x}{x^2+y^2}].

`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
})

describe('asciimath blocks', () => {
  it('explicit asciimath block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath

A matrix can be written as 

[asciimath]
++++
[[a,b],[c,d]]((n),(k))
sqrt(4) = 2
sqrt(9) = 3
++++
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('asciimath prose block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem: latexmath

A matrix can be written as asciimath:[[[a,b\\],[c,d\\]\\]((n),(k))].
A square root looks like asciimath:[sqrt(4) = 2].
Another square root looks like asciimath:[sqrt(9) = 3].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
})
describe('latexmath blocks', () => {
  it('latexmath stem block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:

Some vector components are

[latexmath]
++++
u=\\frac{-y}{x^2+y^2}\\,,\\quad
v=\\frac{x}{x^2+y^2}\\,,\\quad\\text{and}\\quad
w=0\\,.
++++
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('explicit latexmath prose block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:

An integral can be written as latexmath:[\\iint xy^2\\,dx\\,dy ].
A fraction looks like latexmath:[u=\\frac{-y}{x^2+y^2}].
Another fraction looks like latexmath:[v=\\frac{x}{x^2+y^2}].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('fraction with multiline source', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`[latexmath]
++++
\\frac{(byte[{\\tt 8d}] \\times {\\tt 10000} + byte[{\\tt 8e}] \\times
  {\\tt 100} + byte[{\\tt 8f}]) - {\\tt 100000}}{{\\tt 100000}}
++++
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('align test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`:mathjax-tex-packages: ams
    
[latexmath]
++++
\\begin{aligned}
c & =
  \\begin{cases}
    12.91 & c \\leq 0.04 \\\\
    13.2 & \\text{otherwise}
  \\end{cases}
\\end{aligned}
++++
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
})

describe('tex tags tests', () => {
  const text = `
[latexmath]
++++
\\begin{aligned}
c & =
  \\begin{cases}
    12.91 & c \\leq 0.04 \\\\
    13.2 & \\text{otherwise}
  \\end{cases}
\\end{aligned}
++++

[latexmath]
++++
\\begin{equation}
\\begin{aligned}
c & =
  \\begin{cases}
    12.91 & c \\leq 0.04 \\\\
    13.2 & \\text{otherwise}
  \\end{cases}
\\end{aligned}
\\end{equation}
++++
`

  it('tags none', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`:mathjax-tex-packages: ams
:mathjax-tex-tags: none
    
${text}
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('tags ams', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`:mathjax-tex-packages: ams
:mathjax-tex-tags: ams
    
${text}
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })

  it('tags all', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`:mathjax-tex-packages: ams
:mathjax-tex-tags: all
    
${text}
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
})

describe('reset attribute value tests', () => {
  it('asciidoc default prose block with attribute substitution test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
:stem:
:expr: sqrt(4)=2

// A matrix can be written as stem:a[[[{a},{b}\\],[{c},{d}\\]\\](({n}),({k}))].
A square root looks like stem:a[{expr}].

:expr: sqrt(9)=3

Another square root looks like stem:a[{expr}].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
})
describe('stem attribute not required', () => {
  it('asciidoc default prose block test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`

A matrix can be written as stem:[[[a,b\\],[c,d\\]\\]((n),(k))].
A square root looks like stem:[sqrt(4) = 2].
Another square root looks like stem:[sqrt(9) = 3].
`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
})

describe('latexmath inline tests', () => {
  it('fraction with multiple escaped close brackets', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert('latexmath:[\\frac{byte[{\\tt 2e}\\] \\times 256 + byte[{\\tt 2f}\\]}{100.0}]', { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
  it('cfrac dfrac test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(':mathjax-tex-packages: ams\n\nlatexmath:[P_0=\\cfrac{2}{1+\\cfrac{2}{1+\\cfrac{2}{1}}}]', { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
  it('physics test', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(':mathjax-tex-packages: physics\n\nlatexmath:[\\ket{e}]', { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
})

describe('asciidoc table cell attribute reset tests', () => {
  it('asciidoc cell table with head, foot test with cell stem attribute', () => {
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
// :stem: latexmath

.Math Table
[cols="3*a",options="header,footer"]
|===
|:stem: latexmath
Header integral can be written as stem:[\\iint xy^2\\,dx\\,dy ].
stem: {stem}
|:stem: asciimath

stem: {stem}
Header square root: stem:[sqrt(4) = 2]
|:stem: latexmath
Header another fraction looks like stem:[v=\\frac{x}{x^2+y^2}].
stem: {stem}

|A matrix can be written as 

:stem: asciimath

stem: {stem}
stem:[[[a,b\\],[c,d\\]\\]((n),(k))].
|:stem: latexmath
A fraction looks like stem:[u=\\frac{-y}{x^2+y^2}].
stem: {stem}
|
:stem: asciimath
stem: {stem}
Another square root looks like stem:[sqrt(9) = 3]

|:stem: latexmath
Footer integral can be written as stem:[\\iint xy^2\\,dx\\,dy ].
|
:stem: asciimath
Footer square root: stem:[sqrt(4) = 2]
|:stem: latexmath
Footer another fraction looks like stem:[v=\\frac{x}{x^2+y^2}].

|===

`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
  it('asciidoc cell table with foot test with cell content attribute', () => {
    //header cells cannot be asciidoc format nor have attributes.
    const registry = mathJaxProcessor.register(asciidoctor.Extensions.create())
    const html = asciidoctor.convert(`
// :stem: latexmath

.Math Table
[cols="3*a",options="footer"]
|===
|:expr: \\iint xy^2\\,dx\\,dy
Header integral can be written as latexmath:a[{expr}].
|:expr: sqrt(4) = 2
Header square root: asciimath:a[{expr}]
|:expr: v=\\frac{x}{x^2+y^2}
Header another fraction looks like latexmath:a[{expr}].

|A matrix can be written as 

:expr: [[a,b\\],[c,d\\]\\]((n),(k))

stem:a[{expr}].
|:expr: u=\\frac{-y}{x^2+y^2}
A fraction looks like latexmath:a[{expr}].
|
:expr: sqrt(9) = 3

* Another square root looks like stem:a[{expr}]

:expr: sqrt(4) = 2

* Another square root looks like stem:a[{expr}]
* Another square root looks like stem:a[{expr}]

|:expr: \\iint xy^2\\,dx\\,dy
Footer integral can be written as latexmath:a[{expr}].
|
:expr: sqrt(4) = 2
expr: {expr}
Footer square root: stem:a[{expr}]
|:expr: v=\\frac{x}{x^2+y^2}
Footer another fraction looks like latexmath:a[{expr}].

|===

`, { extension_registry: registry })
    if (PRINT) console.log(html)
    expect(html).to.matchSnapshot()
  })
})
